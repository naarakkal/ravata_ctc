# CTC Interface Application

import wx
import wx.lib.scrolledpanel as scrolled
import wx.lib.agw.shapedbutton as SB
import appmail
import time

WINDOWSIZE_X = 500
WINDOWSIZE_Y = 300

USERNAME = 'ravata'
PASSWORD = 'ravata'

EMAIL_ADDRESS = "ravatasolutions@gmail.com"

class LoginPanel(wx.Panel):

    def __init__(self, parent, id):

        wx.Panel.__init__(self, parent=parent, id=id)
        self.__do_layout()

    def __do_layout(self):

        Title = wx.StaticText(self, wx.ID_ANY, "Login", style=wx.ALIGN_CENTER)
        Title.SetMinSize((200, 80))
        Title.SetFont(wx.Font(45, wx.DEFAULT, wx.NORMAL, wx.NORMAL, 0, ""))

        UsernameTitle = wx.StaticText(self, wx.ID_ANY, "Username", style=wx.ALIGN_CENTER)
        UsernameTitle.SetFont(wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.NORMAL, 0, ""))
        PasswordTitle = wx.StaticText(self, wx.ID_ANY, "Password", style=wx.ALIGN_CENTER)
        PasswordTitle.SetFont(wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.NORMAL, 0, ""))

        self.LoginButton = wx.Button(self, wx.ID_ANY, "Login")
        self.UsernameText = wx.TextCtrl(self, size=(140, -1))
        self.PasswordText = wx.TextCtrl(self, size=(140, -1), style = wx.TE_PASSWORD | wx.TE_PROCESS_ENTER)

        SplitSizer = wx.BoxSizer(wx.VERTICAL)

        SplitSizer.Add((0, 20), 0, 0, 0)
        SplitSizer.Add(Title, 0, wx.ALIGN_CENTER, 0)
        SplitSizer.Add(UsernameTitle, 0, wx.ALIGN_CENTER, 0)
        SplitSizer.Add(self.UsernameText, 0, wx.ALIGN_CENTER, 0)
        SplitSizer.Add(PasswordTitle, 0, wx.ALIGN_CENTER, 0)
        SplitSizer.Add(self.PasswordText, 0, wx.ALIGN_CENTER, 0)
        SplitSizer.Add((0, 10), 0, 0, 0)
        SplitSizer.Add(self.LoginButton, 0, wx.ALIGN_CENTER, 0)

        self.SetSizer(SplitSizer)

class HomePanel(wx.Panel):

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent=parent, id=id)
        self.__do_layout()

    def __do_layout(self):

        Title = wx.StaticText(self, wx.ID_ANY, "Home", style=wx.ALIGN_CENTER)
        Title.SetMinSize((200, 85))
        Title.SetFont(wx.Font(45, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))

        userinfobmp = wx.Bitmap("./buttons/userinfo.jpg", wx.BITMAP_TYPE_ANY)
        ctcbmp = wx.Bitmap("./buttons/ctc.jpg", wx.BITMAP_TYPE_ANY)

        self.UserInfoButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=userinfobmp, size=(160, 50))
        self.CTCButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=ctcbmp, size=(160, 50))

        SplitSizer = wx.BoxSizer(wx.VERTICAL)
        ButtonSizer = wx.BoxSizer(wx.HORIZONTAL)

        SplitSizer.Add((0, 45), 0, 0, 0)
        SplitSizer.Add(Title, 1, wx.ALIGN_CENTER, 0)
        ButtonSizer.Add((50, 0), 0, 0, 0)
        ButtonSizer.Add(self.UserInfoButton, 0, 0, 0)
        ButtonSizer.Add((80, 0), 0, 0, 0)
        ButtonSizer.Add(self.CTCButton, 0, 0, 0)
        SplitSizer.Add(ButtonSizer, 20, wx.EXPAND, 0)

        self.SetSizer(SplitSizer)
        SplitSizer.Fit(self)
        self.Layout()

class UserInfoPanel(wx.Panel):

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent=parent, id=id)
        self.__do_layout()

    def __do_layout(self):

        Title = wx.StaticText(self, wx.ID_ANY, "User Info", style=wx.ALIGN_CENTER)
        Title.SetMinSize((400, 90))
        Title.SetFont(wx.Font(45, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))

        LastLogin = wx.StaticText(self, wx.ID_ANY, "Last Login: 11:14AM, 08/28/2018", style=wx.ALIGN_LEFT)
        AccessLevel = wx.StaticText(self, wx.ID_ANY, "Access Level: Administrator", style=wx.ALIGN_LEFT)
        self.NotificationEmail = wx.StaticText(self, wx.ID_ANY, "Email: " + EMAIL_ADDRESS, style=wx.ALIGN_LEFT)
        Manager = wx.StaticText(self, wx.ID_ANY, "Manager: N/A", style=wx.ALIGN_LEFT)

        LastLogin.SetFont(wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.NORMAL, 0, ""))
        AccessLevel.SetFont(wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.NORMAL, 0, ""))
        self.NotificationEmail.SetFont(wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.NORMAL, 0, ""))
        Manager.SetFont(wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.NORMAL, 0, ""))

        backbmp = wx.Bitmap("./buttons/back.jpg", wx.BITMAP_TYPE_ANY)
        homebmp = wx.Bitmap("./buttons/home.jpg", wx.BITMAP_TYPE_ANY)
        adjustsettingsbmp = wx.Bitmap("./buttons/adjustsettings.jpg", wx.BITMAP_TYPE_ANY)

        self.BackButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=backbmp, size=(35, 35))
        self.HomeButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=homebmp, size=(35, 35))

        self.AdjustSettingsButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=adjustsettingsbmp, size=(120, 90))

        TextSizer = wx.BoxSizer(wx.VERTICAL)
        TextSizer.Add((0, 8), 0, 0, 0)
        TextSizer.Add(LastLogin, 0, 0, 0)
        TextSizer.Add(AccessLevel, 0, 0, 0)
        TextSizer.Add(self.NotificationEmail, 0, 0, 0)
        TextSizer.Add(Manager, 0, 0, 0)

        ButtonSizer = wx.BoxSizer(wx.VERTICAL)
        ButtonSizer.Add(self.AdjustSettingsButton, 0, wx.ALIGN_RIGHT, 0)

        MidSizer = wx.BoxSizer(wx.HORIZONTAL)
        MidSizer.Add(TextSizer, 0, 0, 0)
        MidSizer.Add((30, 0), 0, 0, 0)
        MidSizer.Add(ButtonSizer, 0, 0, 0)

        MainSizer = wx.BoxSizer(wx.VERTICAL)
        MainSizer.Add((0, 20), 0, 0, 0)
        MainSizer.Add(Title, 0, wx.ALIGN_CENTER, 0)
        MainSizer.Add(MidSizer, 1, wx.ALIGN_CENTER, 0)

        MainSizer.Add((0, 40), 0, 0, 0)

        NavigationSizer = wx.BoxSizer(wx.HORIZONTAL)
        NavigationSizer.Add((5, 40), 0, 0, 0)
        NavigationSizer.Add(self.BackButton, 1, wx.BOTTOM | wx.LEFT, 0)
        NavigationSizer.Add((420, 0), 0, 0, 0)
        NavigationSizer.Add(self.HomeButton, 1, wx.BOTTOM | wx.RIGHT, 0)
        MainSizer.Add(NavigationSizer, 1, wx.BOTTOM | wx.LEFT, 0)

        self.SetSizer(MainSizer)
        self.Layout()

class AdjustSettingsPanel(wx.Panel):

    def __init__(self, parent, id):

        wx.Panel.__init__(self, parent=parent, id=id)
        self.__do_layout()

    def __do_layout(self):

        Title = wx.StaticText(self, wx.ID_ANY, "User Settings", style=wx.ALIGN_CENTER)
        Title.SetMinSize((400, 85))
        Title.SetFont(wx.Font(45, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))

        EmailPrompt = wx.StaticText(self, wx.ID_ANY, "Enter A Valid Email Address", style=wx.ALIGN_CENTER)
        EmailPrompt.SetFont(wx.Font(16, wx.DEFAULT, wx.NORMAL, wx.NORMAL, 0, ""))

        backbmp = wx.Bitmap("./buttons/back.jpg", wx.BITMAP_TYPE_ANY)
        homebmp = wx.Bitmap("./buttons/home.jpg", wx.BITMAP_TYPE_ANY)

        self.ConfirmButton = wx.Button(self, wx.ID_ANY, "Confirm")
        self.BackButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=backbmp, size=(35, 35))
        self.HomeButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=homebmp, size=(35, 35))
        self.EmailText = wx.TextCtrl(self, size=(240, -1), style = wx.TE_PROCESS_ENTER)

        SplitSizer = wx.BoxSizer(wx.VERTICAL)
        NavigationSizer = wx.BoxSizer(wx.HORIZONTAL)

        SplitSizer.Add((0, 20), 0, 0, 0)
        SplitSizer.Add(Title, 0, wx.ALIGN_CENTER, 0)
        SplitSizer.Add((0, 20), 0, 0, 0)
        SplitSizer.Add(EmailPrompt, 0, wx.ALIGN_CENTER, 0)
        SplitSizer.Add(self.EmailText, 0, wx.ALIGN_CENTER, 0)
        SplitSizer.Add((0, 10), 0, 0, 0)
        SplitSizer.Add(self.ConfirmButton, 0, wx.ALIGN_CENTER, 0)
        SplitSizer.Add((0, 44), 0, 0, 0)

        NavigationSizer.Add((5, 40), 0, 0, 0)
        NavigationSizer.Add(self.BackButton, 1, wx.BOTTOM | wx.LEFT, 0)
        NavigationSizer.Add((420, 0), 0, 0, 0)
        NavigationSizer.Add(self.HomeButton, 1, wx.BOTTOM | wx.RIGHT, 0)
        SplitSizer.Add(NavigationSizer, 1, 0, 0)

        self.SetSizer(SplitSizer)
        self.Layout()

class CTCPanel(wx.Panel):

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent=parent, id=id)
        self.__do_layout()

    def __do_layout(self):

        Title = wx.StaticText(self, wx.ID_ANY, "CTC", style=wx.ALIGN_CENTER)
        Title.SetMinSize((200, 85))
        Title.SetFont(wx.Font(45, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))

        monitorbmp = wx.Bitmap("./buttons/monitor.jpg", wx.BITMAP_TYPE_ANY)
        controlbmp = wx.Bitmap("./buttons/control.jpg", wx.BITMAP_TYPE_ANY)
        backbmp = wx.Bitmap("./buttons/back.jpg", wx.BITMAP_TYPE_ANY)
        homebmp = wx.Bitmap("./buttons/home.jpg", wx.BITMAP_TYPE_ANY)

        self.MonitorButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=monitorbmp, size=(160, 50))
        self.ControlButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=controlbmp, size=(160, 50))
        self.BackButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=backbmp, size=(35, 35))
        self.HomeButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=homebmp, size=(35, 35))

        PanelSizer = wx.BoxSizer(wx.VERTICAL)
        SplitSizer = wx.BoxSizer(wx.VERTICAL)
        ButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
        NavigationSizer = wx.BoxSizer(wx.HORIZONTAL)

        SplitSizer.Add((0, 45), 0, 0, 0)
        SplitSizer.Add(Title, 1, wx.ALIGN_CENTER, 0)

        ButtonSizer.Add((50, 0), 0, 0, 0)
        ButtonSizer.Add(self.MonitorButton, 0, 0, 0)
        ButtonSizer.Add((80, 0), 0, 0, 0)
        ButtonSizer.Add(self.ControlButton, 0, 0, 0)
        SplitSizer.Add(ButtonSizer, 20, wx.EXPAND, 0)

        NavigationSizer.Add((5, 40), 0, 0, 0)
        NavigationSizer.Add(self.BackButton, 1, wx.BOTTOM | wx.LEFT, 0)
        NavigationSizer.Add((420, 0), 0, 0, 0)
        NavigationSizer.Add(self.HomeButton, 1, wx.BOTTOM | wx.RIGHT, 0)

        SplitSizer.Add(NavigationSizer, 1, 0, 0)
        PanelSizer.Add(SplitSizer, 1, wx.EXPAND, 0)

        self.SetSizer(PanelSizer)
        PanelSizer.Fit(self)
        self.Layout()

class MonitorPanel(wx.Panel):

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent=parent, id=id)
        self.__do_layout()

    def __do_layout(self):

        Title = wx.StaticText(self, wx.ID_ANY, "Monitor", style=wx.ALIGN_CENTER)
        Title.SetMinSize((200, 85))
        Title.SetFont(wx.Font(45, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))

        reportsbmp = wx.Bitmap("./buttons/reports.jpg", wx.BITMAP_TYPE_ANY)
        alarmhistorybmp = wx.Bitmap("./buttons/alarmhistory.jpg", wx.BITMAP_TYPE_ANY)
        backbmp = wx.Bitmap("./buttons/back.jpg", wx.BITMAP_TYPE_ANY)
        homebmp = wx.Bitmap("./buttons/home.jpg", wx.BITMAP_TYPE_ANY)

        self.ReportsButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=reportsbmp, size=(160, 70))
        self.AlarmHistoryButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=alarmhistorybmp, size=(160, 70))
        self.BackButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=backbmp, size=(35, 35))
        self.HomeButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=homebmp, size=(35, 35))

        PanelSizer = wx.BoxSizer(wx.VERTICAL)
        SplitSizer = wx.BoxSizer(wx.VERTICAL)
        ButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
        NavigationSizer = wx.BoxSizer(wx.HORIZONTAL)

        SplitSizer.Add((0, 45), 0, 0, 0)
        SplitSizer.Add(Title, 1, wx.ALIGN_CENTER, 0)

        ButtonSizer.Add((50, 0), 0, 0, 0)
        ButtonSizer.Add(self.ReportsButton, 0, 0, 0)
        ButtonSizer.Add((80, 0), 0, 0, 0)
        ButtonSizer.Add(self.AlarmHistoryButton, 0, 0, 0)
        SplitSizer.Add(ButtonSizer, 20, wx.EXPAND, 0)

        NavigationSizer.Add((5, 40), 0, 0, 0)
        NavigationSizer.Add(self.BackButton, 1, wx.BOTTOM | wx.LEFT, 0)
        NavigationSizer.Add((420, 0), 0, 0, 0)
        NavigationSizer.Add(self.HomeButton, 1, wx.BOTTOM | wx.RIGHT, 0)
        SplitSizer.Add(NavigationSizer, 1, 0, 0)
        PanelSizer.Add(SplitSizer, 1, wx.EXPAND, 0)

        self.SetSizer(PanelSizer)
        PanelSizer.Fit(self)
        self.Layout()

class AlarmHistoryPanel(wx.Panel):

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent=parent, id=id)

        bg = './bg/mainbg_history.jpg'
        bitmap = wx.Image(bg, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.background = wx.StaticBitmap(self, -1, bitmap, (0, 0))

        self.AlarmHistory = ""
        backbmp = wx.Bitmap("./buttons/back.jpg", wx.BITMAP_TYPE_ANY)
        homebmp = wx.Bitmap("./buttons/home.jpg", wx.BITMAP_TYPE_ANY)
        emailhistorybmp = wx.Bitmap("./buttons/emailhistory.jpg", wx.BITMAP_TYPE_ANY)
        self.BackButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=backbmp, size=(35, 35))
        self.HomeButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=homebmp, size=(35, 35))
        self.EmailHistoryButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=emailhistorybmp, size=(110, 100))
        self.__do_layout()

    def __do_layout(self):
        Title = wx.StaticText(self, wx.ID_ANY, "Alarm History", style=wx.ALIGN_CENTER)
        Title.SetMinSize((400, 0))
        Title.SetFont(wx.Font(45, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))

        self.AlarmHistoryText = wx.TextCtrl(self, size=(300, 100), style = wx.TE_MULTILINE)

        PanelSizer = wx.BoxSizer(wx.VERTICAL)
        SplitSizer = wx.BoxSizer(wx.VERTICAL)
        NavigationSizer = wx.BoxSizer(wx.HORIZONTAL)
        MidSizer = wx.BoxSizer(wx.HORIZONTAL)
        ButtonSizer = wx.BoxSizer(wx.VERTICAL)

        SplitSizer.Add((0, 20), 0, 0, 0)
        SplitSizer.Add(Title, 1, wx.ALIGN_CENTER, 0)

        MidSizer.Add((40, 0), 0, 0, 0)
        MidSizer.Add(self.AlarmHistoryText, 0, 0)
        MidSizer.Add((10, 0), 0, 0, 0)
        ButtonSizer.Add((0, 0), 0, 0, 0)
        ButtonSizer.Add(self.EmailHistoryButton, 0, 0)
        MidSizer.Add(ButtonSizer, 0, 0)
        SplitSizer.Add(MidSizer, 1, 0, 0)

        SplitSizer.Add((0, 20), 0, 0, 0)
        NavigationSizer.Add((5, 40), 0, 0, 0)
        NavigationSizer.Add(self.BackButton, 1, wx.BOTTOM | wx.LEFT, 0)
        NavigationSizer.Add((420, 0), 0, 0, 0)
        NavigationSizer.Add(self.HomeButton, 1, wx.BOTTOM | wx.RIGHT, 0)
        SplitSizer.Add(NavigationSizer, 1, 0, 0)

        PanelSizer.Add(SplitSizer, 1, wx.EXPAND, 0)
        self.SetSizer(PanelSizer)
        PanelSizer.Fit(self)
        self.Layout()

    def updateAlarmHistory(self):
        self.AlarmHistoryText.SetValue(self.AlarmHistory)

class ControlPanel(wx.Panel):

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent=parent, id=id)
        self.__do_layout()

    def __do_layout(self):
        Title = wx.StaticText(self, wx.ID_ANY, "Control", style=wx.ALIGN_CENTER)
        Title.SetMinSize((200, 85))
        Title.SetFont(wx.Font(45, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))

        filllevelbmp = wx.Bitmap("./buttons/filllevel.jpg", wx.BITMAP_TYPE_ANY)
        alarmsbmp = wx.Bitmap("./buttons/alarms.jpg", wx.BITMAP_TYPE_ANY)
        backbmp = wx.Bitmap("./buttons/back.jpg", wx.BITMAP_TYPE_ANY)
        homebmp = wx.Bitmap("./buttons/home.jpg", wx.BITMAP_TYPE_ANY)

        self.FillLevelButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=filllevelbmp, size=(160, 50))
        self.AlarmsControlButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=alarmsbmp, size=(160, 50))
        self.BackButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=backbmp, size=(35, 35))
        self.HomeButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=homebmp, size=(35, 35))

        PanelSizer = wx.BoxSizer(wx.VERTICAL)
        SplitSizer = wx.BoxSizer(wx.VERTICAL)
        ButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
        NavigationSizer = wx.BoxSizer(wx.HORIZONTAL)

        SplitSizer.Add((0, 45), 0, 0, 0)
        SplitSizer.Add(Title, 1, wx.ALIGN_CENTER, 0)

        ButtonSizer.Add((50, 0), 0, 0, 0)
        ButtonSizer.Add(self.FillLevelButton, 0, 0, 0)
        ButtonSizer.Add((80, 0), 0, 0, 0)
        ButtonSizer.Add(self.AlarmsControlButton, 0, 0, 0)
        SplitSizer.Add(ButtonSizer, 20, wx.EXPAND, 0)

        NavigationSizer.Add((5, 40), 0, 0, 0)
        NavigationSizer.Add(self.BackButton, 1, wx.BOTTOM | wx.LEFT, 0)
        NavigationSizer.Add((420, 0), 0, 0, 0)
        NavigationSizer.Add(self.HomeButton, 1, wx.BOTTOM | wx.RIGHT, 0)
        SplitSizer.Add(NavigationSizer, 1, 0, 0)
        PanelSizer.Add(SplitSizer, 1, wx.EXPAND, 0)

        self.SetSizer(PanelSizer)
        PanelSizer.Fit(self)
        self.Layout()

class ReportsPanel(wx.Panel):

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent=parent, id=id)
        self.__do_layout()

    def __do_layout(self):
        Title = wx.StaticText(self, wx.ID_ANY, "Reports", style=wx.ALIGN_CENTER)
        Title.SetMinSize((200, 75))
        Title.SetFont(wx.Font(45, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))

        tempreportbmp = backbmp = wx.Bitmap("./buttons/tempreport.jpg", wx.BITMAP_TYPE_ANY)
        fillreportbmp = backbmp = wx.Bitmap("./buttons/fillreport.jpg", wx.BITMAP_TYPE_ANY)
        alarmreportbmp = backbmp = wx.Bitmap("./buttons/alarmreport.jpg", wx.BITMAP_TYPE_ANY)
        backbmp = wx.Bitmap("./buttons/back.jpg", wx.BITMAP_TYPE_ANY)
        homebmp = wx.Bitmap("./buttons/home.jpg", wx.BITMAP_TYPE_ANY)

        self.TempReportButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=tempreportbmp, size=(120, 100))
        self.FillReportButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=fillreportbmp, size=(120, 100))
        self.AlarmReportButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=alarmreportbmp, size=(120, 100))
        self.BackButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=backbmp, size=(35, 35))
        self.HomeButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=homebmp, size=(35, 35))

        PanelSizer = wx.BoxSizer(wx.VERTICAL)
        SplitSizer = wx.BoxSizer(wx.VERTICAL)
        ButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
        NavigationSizer = wx.BoxSizer(wx.HORIZONTAL)

        SplitSizer.Add((0, 35), 0, 0, 0)
        SplitSizer.Add(Title, 1, wx.ALIGN_CENTER, 0)
        ButtonSizer.Add((50, 0), 0, 0, 0)
        ButtonSizer.Add(self.TempReportButton, 0, 0, 0)
        ButtonSizer.Add((20, 0), 0, 0, 0)
        ButtonSizer.Add(self.FillReportButton, 0, 0, 0)
        ButtonSizer.Add((20, 0), 0, 0, 0)
        ButtonSizer.Add(self.AlarmReportButton, 0, 0, 0)
        SplitSizer.Add(ButtonSizer, 1, wx.EXPAND, 0)

        SplitSizer.Add((0, 30), 0, 0, 0)
        NavigationSizer.Add((5, 40), 0, 0, 0)
        NavigationSizer.Add(self.BackButton, 1, wx.BOTTOM | wx.LEFT, 0)
        NavigationSizer.Add((420, 0), 0, 0, 0)
        NavigationSizer.Add(self.HomeButton, 1, wx.BOTTOM | wx.RIGHT, 0)

        SplitSizer.Add(NavigationSizer, 1, 0, 0)
        PanelSizer.Add(SplitSizer, 1, wx.EXPAND, 0)

        self.SetSizer(PanelSizer)
        PanelSizer.Fit(self)
        self.Layout()

class AlarmsControlPanel(wx.Panel):

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent=parent, id=id)
        self.__do_layout()

    def __do_layout(self):
        Title = wx.StaticText(self, wx.ID_ANY, "Alarms", style=wx.ALIGN_CENTER)
        Title.SetMinSize((200, 85))
        Title.SetFont(wx.Font(45, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))

        mutealarmbmp = wx.Bitmap("./buttons/mutealarm.jpg", wx.BITMAP_TYPE_ANY)
        alarmthresholdbmp = wx.Bitmap("./buttons/alarmthresholds.jpg", wx.BITMAP_TYPE_ANY)
        backbmp = wx.Bitmap("./buttons/back.jpg", wx.BITMAP_TYPE_ANY)
        homebmp = wx.Bitmap("./buttons/home.jpg", wx.BITMAP_TYPE_ANY)

        self.MuteAlarmButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=mutealarmbmp, size=(160, 70))
        self.AlarmThresholdButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=alarmthresholdbmp, size=(160, 70))
        self.BackButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=backbmp, size=(35, 35))
        self.HomeButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=homebmp, size=(35, 35))

        PanelSizer = wx.BoxSizer(wx.VERTICAL)
        SplitSizer = wx.BoxSizer(wx.VERTICAL)
        ButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
        NavigationSizer = wx.BoxSizer(wx.HORIZONTAL)

        SplitSizer.Add((0, 45), 0, 0, 0)
        SplitSizer.Add(Title, 1, wx.ALIGN_CENTER, 0)

        ButtonSizer.Add((50, 0), 0, 0, 0)
        ButtonSizer.Add(self.MuteAlarmButton, 0, 0, 0)
        ButtonSizer.Add((80, 0), 0, 0, 0)
        ButtonSizer.Add(self.AlarmThresholdButton, 0, 0, 0)
        SplitSizer.Add(ButtonSizer, 20, wx.EXPAND, 0)

        NavigationSizer.Add((5, 40), 0, 0, 0)
        NavigationSizer.Add(self.BackButton, 1, wx.BOTTOM | wx.LEFT, 0)
        NavigationSizer.Add((420, 0), 0, 0, 0)
        NavigationSizer.Add(self.HomeButton, 1, wx.BOTTOM | wx.RIGHT, 0)
        SplitSizer.Add(NavigationSizer, 1, 0, 0)
        PanelSizer.Add(SplitSizer, 1, wx.EXPAND, 0)

        self.SetSizer(PanelSizer)
        PanelSizer.Fit(self)
        self.Layout()

class AlarmThresholdPanel(wx.Panel):

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent=parent, id=id)

        self.HIGHTEMP_THRESHOLD = -110.0
        self.LOWTEMP_THRESHOLD = -198.0
        self.HIGHLEVEL_THRESHOLD = 8.0
        self.LOWLEVEL_THRESHOLD = 4.0

        self.HighTempValue = wx.StaticText(self, wx.ID_ANY, str(self.HIGHTEMP_THRESHOLD) + "C", style=wx.ALIGN_CENTER)
        self.HighTempValue.SetMinSize((80, 25))
        self.LowTempValue = wx.StaticText(self, wx.ID_ANY, str(self.LOWTEMP_THRESHOLD) + "C", style=wx.ALIGN_CENTER)
        self.LowTempValue.SetMinSize((80, 25))
        self.HighLevelValue = wx.StaticText(self, wx.ID_ANY, str(self.HIGHLEVEL_THRESHOLD) + "in", style=wx.ALIGN_CENTER)
        self.HighLevelValue.SetMinSize((80, 25))
        self.LowLevelValue= wx.StaticText(self, wx.ID_ANY, str(self.LOWLEVEL_THRESHOLD) + "in", style=wx.ALIGN_CENTER)
        self.LowLevelValue.SetMinSize((80, 25))

        plusbmp = wx.Bitmap("./buttons/plus.jpg", wx.BITMAP_TYPE_ANY)
        minusbmp = wx.Bitmap("./buttons/minus.jpg", wx.BITMAP_TYPE_ANY)
        backbmp = wx.Bitmap("./buttons/back.jpg", wx.BITMAP_TYPE_ANY)
        homebmp = wx.Bitmap("./buttons/home.jpg", wx.BITMAP_TYPE_ANY)

        self.HighTempPlus = wx.BitmapButton(self, wx.ID_ANY, bitmap=plusbmp, size=(25, 25))
        self.HighTempMinus = wx.BitmapButton(self, wx.ID_ANY, bitmap=minusbmp, size=(25, 25))
        self.LowTempPlus = wx.BitmapButton(self, wx.ID_ANY, bitmap=plusbmp, size=(25, 25))
        self.LowTempMinus = wx.BitmapButton(self, wx.ID_ANY, bitmap=minusbmp, size=(25, 25))
        self.HighLevelPlus = wx.BitmapButton(self, wx.ID_ANY, bitmap=plusbmp, size=(25, 25))
        self.HighLevelMinus = wx.BitmapButton(self, wx.ID_ANY, bitmap=minusbmp, size=(25, 25))
        self.LowLevelPlus = wx.BitmapButton(self, wx.ID_ANY, bitmap=plusbmp, size=(25, 25))
        self.LowLevelMinus = wx.BitmapButton(self, wx.ID_ANY, bitmap=minusbmp, size=(25, 25))

        self.ConfirmButton = wx.Button(self, wx.ID_ANY, "Confirm")
        self.BackButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=backbmp, size=(35, 35))
        self.HomeButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=homebmp, size=(35, 35))

        self.Bind(wx.EVT_BUTTON, lambda event: self.increaseHighTempValue(event, self.HighTempValue), self.HighTempPlus)
        self.Bind(wx.EVT_BUTTON, lambda event: self.increaseLowTempValue(event, self.LowTempValue), self.LowTempPlus)
        self.Bind(wx.EVT_BUTTON, lambda event: self.increaseHighLevelValue(event, self.HighLevelValue), self.HighLevelPlus)
        self.Bind(wx.EVT_BUTTON, lambda event: self.increaseLowLevelValue(event, self.LowLevelValue), self.LowLevelPlus)

        self.Bind(wx.EVT_BUTTON, lambda event: self.decreaseHighTempValue(event, self.HighTempValue), self.HighTempMinus)
        self.Bind(wx.EVT_BUTTON, lambda event: self.decreaseLowTempValue(event, self.LowTempValue), self.LowTempMinus)
        self.Bind(wx.EVT_BUTTON, lambda event: self.decreaseHighLevelValue(event, self.HighLevelValue), self.HighLevelMinus)
        self.Bind(wx.EVT_BUTTON, lambda event: self.decreaseLowLevelValue(event, self.LowLevelValue), self.LowLevelMinus)

        self.__do_layout()

    def __do_layout(self):
        Title = wx.StaticText(self, wx.ID_ANY, "Alarm Thresholds", style=wx.ALIGN_CENTER)
        Title.SetMinSize((450, 70))
        Title.SetFont(wx.Font(45, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))

        HighTempTitle = wx.StaticText(self, wx.ID_ANY, "High Temp Threshold", style=wx.ALIGN_CENTER)
        HighTempTitle.SetMinSize((150, 25))
        LowTempTitle = wx.StaticText(self, wx.ID_ANY, "Low Temp Threshold", style=wx.ALIGN_CENTER)
        LowTempTitle.SetMinSize((150, 25))
        HighLevelTitle = wx.StaticText(self, wx.ID_ANY, "High Level Threshold", style=wx.ALIGN_CENTER)
        HighLevelTitle.SetMinSize((150, 25))
        LowLevelTitle = wx.StaticText(self, wx.ID_ANY, "Low Level Threshold", style=wx.ALIGN_CENTER)
        LowLevelTitle.SetMinSize((150, 25))

        PanelSizer = wx.BoxSizer(wx.VERTICAL)
        SplitSizer = wx.BoxSizer(wx.VERTICAL)

        HighTempSizer = wx.BoxSizer(wx.HORIZONTAL)
        LowTempSizer = wx.BoxSizer(wx.HORIZONTAL)
        HighLevelSizer = wx.BoxSizer(wx.HORIZONTAL)
        LowLevelSizer = wx.BoxSizer(wx.HORIZONTAL)

        NavigationSizer = wx.BoxSizer(wx.HORIZONTAL)

        SplitSizer.Add((0, 30), 0, 0, 0)
        SplitSizer.Add(Title, 1, wx.ALIGN_CENTER, 0)

        HighTempSizer.Add((95, 0), 0, 0, 0)
        HighTempSizer.Add(HighTempTitle, 0, 0, 0)
        HighTempSizer.Add((30, 0), 0, 0, 0)
        HighTempSizer.Add(self.HighTempMinus, 0, 0, 0)
        HighTempSizer.Add(self.HighTempValue, 0, 0, 0)
        HighTempSizer.Add(self.HighTempPlus, 0, 0, 0)
        SplitSizer.Add(HighTempSizer, 0, 0, 0)

        LowTempSizer.Add((95, 0), 0, 0, 0)
        LowTempSizer.Add(LowTempTitle, 0, 0, 0)
        LowTempSizer.Add((30, 0), 0, 0, 0)
        LowTempSizer.Add(self.LowTempMinus, 0, 0, 0)
        LowTempSizer.Add(self.LowTempValue, 0, 0, 0)
        LowTempSizer.Add(self.LowTempPlus, 0, 0, 0)
        SplitSizer.Add(LowTempSizer, 0, 0, 0)

        HighLevelSizer.Add((95, 0), 0, 0, 0)
        HighLevelSizer.Add(HighLevelTitle, 0, 0, 0)
        HighLevelSizer.Add((30, 0), 0, 0, 0)
        HighLevelSizer.Add(self.HighLevelMinus, 0, 0, 0)
        HighLevelSizer.Add(self.HighLevelValue, 0, 0, 0)
        HighLevelSizer.Add(self.HighLevelPlus, 0, 0, 0)
        SplitSizer.Add(HighLevelSizer, 0, 0, 0)

        LowLevelSizer.Add((95, 0), 0, 0, 0)
        LowLevelSizer.Add(LowLevelTitle, 0, 0, 0)
        LowLevelSizer.Add((30, 0), 0, 0, 0)
        LowLevelSizer.Add(self.LowLevelMinus, 0, 0, 0)
        LowLevelSizer.Add(self.LowLevelValue, 0, 0, 0)
        LowLevelSizer.Add(self.LowLevelPlus, 0, 0, 0)
        SplitSizer.Add(LowLevelSizer, 0, 0, 0)

        SplitSizer.Add((0, 10), 0, 0, 0)
        SplitSizer.Add(self.ConfirmButton, 1, wx.ALIGN_CENTER, 0)

        NavigationSizer.Add((5, 40), 0, 0, 0)
        NavigationSizer.Add(self.BackButton, 1, wx.BOTTOM | wx.LEFT, 0)
        NavigationSizer.Add((420, 0), 0, 0, 0)
        NavigationSizer.Add(self.HomeButton, 1, wx.BOTTOM | wx.RIGHT, 0)
        SplitSizer.Add(NavigationSizer, 1, 0, 0)

        PanelSizer.Add(SplitSizer, 1, wx.EXPAND, 0)
        self.SetSizer(PanelSizer)
        PanelSizer.Fit(self)
        self.Layout()

    def increaseHighTempValue(self, event, label):
        self.HIGHTEMP_THRESHOLD += 1
        label.SetLabel(str(self.HIGHTEMP_THRESHOLD) + "C")

    def increaseLowTempValue(self, event, label):
        self.LOWTEMP_THRESHOLD += 1
        label.SetLabel(str(self.LOWTEMP_THRESHOLD) + "C")

    def increaseHighLevelValue(self, event, label):
        self.HIGHLEVEL_THRESHOLD += 0.1
        label.SetLabel(str(self.HIGHLEVEL_THRESHOLD) + "in")

    def increaseLowLevelValue(self, event, label):
        self.LOWLEVEL_THRESHOLD += 0.1
        label.SetLabel(str(self.LOWLEVEL_THRESHOLD) + "in")

    def decreaseHighTempValue(self, event, label):
        self.HIGHTEMP_THRESHOLD -= 1
        label.SetLabel(str(self.HIGHTEMP_THRESHOLD) + "C")

    def decreaseLowTempValue(self, event, label):
        self.LOWTEMP_THRESHOLD -= 1
        label.SetLabel(str(self.LOWTEMP_THRESHOLD) + "C")

    def decreaseHighLevelValue(self, event, label):
        self.HIGHLEVEL_THRESHOLD -= 0.1
        label.SetLabel(str(self.HIGHLEVEL_THRESHOLD) + "in")

    def decreaseLowLevelValue(self, event, label):
        self.LOWLEVEL_THRESHOLD -= 0.1
        label.SetLabel(str(self.LOWLEVEL_THRESHOLD) + "in")

    def updateThresholdLabels(self):
        self.HighTempValue.SetLabel(str(self.HIGHTEMP_THRESHOLD) + "C")
        self.LowTempValue.SetLabel(str(self.LOWTEMP_THRESHOLD) + "C")
        self.HighLevelValue.SetLabel(str(self.HIGHLEVEL_THRESHOLD) + "in")
        self.LowLevelValue.SetLabel(str(self.LOWLEVEL_THRESHOLD) + "in")

class FillLevelPanel(wx.Panel):

    def __init__(self, parent, id):

        wx.Panel.__init__(self, parent=parent, id=id)
        self.__do_layout()

    def __do_layout(self):

        Title = wx.StaticText(self, wx.ID_ANY, "Fill Level", style=wx.ALIGN_CENTER)
        Title.SetMinSize((200, 80))
        Title.SetFont(wx.Font(35, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))

        startfillbmp = wx.Bitmap("./buttons/startfill.jpg", wx.BITMAP_TYPE_ANY)
        stopfillbmp = wx.Bitmap("./buttons/stopfill.jpg", wx.BITMAP_TYPE_ANY)
        backbmp = wx.Bitmap("./buttons/back.jpg", wx.BITMAP_TYPE_ANY)
        homebmp = wx.Bitmap("./buttons/home.jpg", wx.BITMAP_TYPE_ANY)

        self.StartFillButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=startfillbmp, size=(160, 50))
        self.StopFillButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=stopfillbmp, size=(160, 50))
        self.BackButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=backbmp, size=(35, 35))
        self.HomeButton = wx.BitmapButton(self, wx.ID_ANY, bitmap=homebmp, size=(35, 35))

        PanelSizer = wx.BoxSizer(wx.VERTICAL)
        SplitSizer = wx.BoxSizer(wx.VERTICAL)
        ButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
        NavigationSizer = wx.BoxSizer(wx.HORIZONTAL)

        SplitSizer.Add((0, 50), 0, 0, 0)
        SplitSizer.Add(Title, 1, wx.ALIGN_CENTER, 0)

        ButtonSizer.Add((50, 0), 0, 0, 0)
        ButtonSizer.Add(self.StartFillButton, 0, 0, 0)
        ButtonSizer.Add((80, 0), 0, 0, 0)
        ButtonSizer.Add(self.StopFillButton, 0, 0, 0)
        SplitSizer.Add(ButtonSizer, 1, wx.EXPAND, 0)

        SplitSizer.Add((0, 28), 0, 0, 0)
        NavigationSizer.Add((5, 40), 0, 0, 0)
        NavigationSizer.Add(self.BackButton, 1, wx.BOTTOM | wx.LEFT, 0)
        NavigationSizer.Add((420, 0), 0, 0, 0)
        NavigationSizer.Add(self.HomeButton, 1, wx.BOTTOM | wx.RIGHT, 0)

        SplitSizer.Add(NavigationSizer, 1, 0, 0)
        PanelSizer.Add(SplitSizer, 1, wx.EXPAND, 0)

        self.SetSizer(PanelSizer)
        PanelSizer.Fit(self)
        self.Layout()

class MainFrame(wx.Frame):

    def __init__(self, *args, **kwds):

        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.SetSize((WINDOWSIZE_X, WINDOWSIZE_Y))

        try:
            bg = './bg/mainbg.jpg'
            bitmap = wx.Image(bg, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            self.background = wx.StaticBitmap(self, -1, bitmap, (0, 0))
        except IOError:
            print "Background image not found."

        #CHANGE HERE
        self.LoginPanel = LoginPanel(self, wx.ID_ANY)
        self.HomePanel = HomePanel(self, wx.ID_ANY)
        self.UserInfoPanel = UserInfoPanel(self, wx.ID_ANY)
        self.AdjustSettingsPanel = AdjustSettingsPanel(self, wx.ID_ANY)
        self.CTCPanel = CTCPanel(self, wx.ID_ANY)
        self.MonitorPanel = MonitorPanel(self, wx.ID_ANY)
        self.ControlPanel = ControlPanel(self, wx.ID_ANY)
        self.ReportsPanel = ReportsPanel(self, wx.ID_ANY)
        self.AlarmsControlPanel = AlarmsControlPanel(self, wx.ID_ANY)
        self.FillLevelPanel = FillLevelPanel(self, wx.ID_ANY)
        self.AlarmThresholdPanel = AlarmThresholdPanel(self, wx.ID_ANY)
        self.AlarmHistoryPanel = AlarmHistoryPanel(self, wx.ID_ANY)

        self.LoginPanel.Hide()
        #self.HomePanel.Hide()
        self.UserInfoPanel.Hide()
        self.AdjustSettingsPanel.Hide()
        self.CTCPanel.Hide()
        self.MonitorPanel.Hide()
        self.ControlPanel.Hide()
        self.ReportsPanel.Hide()
        self.AlarmsControlPanel.Hide()
        self.FillLevelPanel.Hide()
        self.AlarmThresholdPanel.Hide()
        self.AlarmHistoryPanel.Hide()

        self.__set_properties()
        self.__do_layout()

        self.currentPanel = self.HomePanel
        self.prevPanel = self.HomePanel
        self.prev2Panel = self.HomePanel
        self.prev3Panel = self.HomePanel
        self.prev4Panel = self.HomePanel

        self.Bind(wx.EVT_BUTTON, self.back, self.CTCPanel.BackButton)
        self.Bind(wx.EVT_BUTTON, self.back, self.UserInfoPanel.BackButton)
        self.Bind(wx.EVT_BUTTON, self.back, self.AdjustSettingsPanel.BackButton)
        self.Bind(wx.EVT_BUTTON, self.back, self.MonitorPanel.BackButton)
        self.Bind(wx.EVT_BUTTON, self.back, self.ControlPanel.BackButton)
        self.Bind(wx.EVT_BUTTON, self.back, self.ReportsPanel.BackButton)
        self.Bind(wx.EVT_BUTTON, self.back, self.AlarmsControlPanel.BackButton)
        self.Bind(wx.EVT_BUTTON, self.back, self.FillLevelPanel.BackButton)
        self.Bind(wx.EVT_BUTTON, self.back, self.AlarmThresholdPanel.BackButton)
        self.Bind(wx.EVT_BUTTON, self.back, self.AlarmHistoryPanel.BackButton)

        self.Bind(wx.EVT_BUTTON, self.home, self.CTCPanel.HomeButton)
        self.Bind(wx.EVT_BUTTON, self.home, self.UserInfoPanel.HomeButton)
        self.Bind(wx.EVT_BUTTON, self.home, self.MonitorPanel.HomeButton)
        self.Bind(wx.EVT_BUTTON, self.home, self.AdjustSettingsPanel.HomeButton)
        self.Bind(wx.EVT_BUTTON, self.home, self.ControlPanel.HomeButton)
        self.Bind(wx.EVT_BUTTON, self.home, self.ReportsPanel.HomeButton)
        self.Bind(wx.EVT_BUTTON, self.home, self.AlarmsControlPanel.HomeButton)
        self.Bind(wx.EVT_BUTTON, self.home, self.FillLevelPanel.HomeButton)
        self.Bind(wx.EVT_BUTTON, self.home, self.AlarmThresholdPanel.HomeButton)
        self.Bind(wx.EVT_BUTTON, self.home, self.AlarmHistoryPanel.HomeButton)

        self.Bind(wx.EVT_BUTTON, self.login, self.LoginPanel.LoginButton)
        self.Bind(wx.EVT_TEXT_ENTER, self.login, self.LoginPanel.PasswordText)

        self.Bind(wx.EVT_BUTTON, self.changeEmailAddress, self.AdjustSettingsPanel.ConfirmButton)
        self.Bind(wx.EVT_TEXT_ENTER, self.changeEmailAddress, self.AdjustSettingsPanel.EmailText)

        self.Bind(wx.EVT_BUTTON, self.sendThresholdSettings, self.AlarmThresholdPanel.ConfirmButton)

        self.Bind(wx.EVT_BUTTON, lambda event: self.switchPanel(event, self.UserInfoPanel), self.HomePanel.UserInfoButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.switchPanel(event, self.AdjustSettingsPanel), self.UserInfoPanel.AdjustSettingsButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.switchPanel(event, self.CTCPanel), self.HomePanel.CTCButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.switchPanel(event, self.MonitorPanel), self.CTCPanel.MonitorButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.switchPanel(event, self.ControlPanel), self.CTCPanel.ControlButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.switchPanel(event, self.ReportsPanel), self.MonitorPanel.ReportsButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.switchPanel(event, self.AlarmsControlPanel), self.ControlPanel.AlarmsControlButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.switchPanel(event, self.FillLevelPanel), self.ControlPanel.FillLevelButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.switchPanel(event, self.AlarmThresholdPanel), self.AlarmsControlPanel.AlarmThresholdButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.switchPanel(event, self.AlarmHistoryPanel), self.MonitorPanel.AlarmHistoryButton)

        self.Bind(wx.EVT_BUTTON, lambda event: self.sendCommand(event, "SENDTEMPREPORT"), self.ReportsPanel.TempReportButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.sendCommand(event, "SENDFILLREPORT"), self.ReportsPanel.FillReportButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.sendCommand(event, "SENDALARMREPORT"), self.ReportsPanel.AlarmReportButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.sendCommand(event, "MUTEALARM"), self.AlarmsControlPanel.MuteAlarmButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.sendCommand(event, "STARTFILL"), self.FillLevelPanel.StartFillButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.sendCommand(event, "STOPFILL"), self.FillLevelPanel.StopFillButton)
        self.Bind(wx.EVT_BUTTON, lambda event: self.sendCommand(event, "SENDHISTORY"), self.AlarmHistoryPanel.EmailHistoryButton)

    def __set_properties(self):
        self.SetTitle("CTC Interface Application")

    def __do_layout(self):

        MainFrameSizer = wx.BoxSizer(wx.VERTICAL)

        MainFrameSizer.Add(self.LoginPanel, 0, wx.EXPAND, 0)
        MainFrameSizer.Add(self.HomePanel, 0, wx.EXPAND, 0)
        MainFrameSizer.Add(self.UserInfoPanel, 0, wx.EXPAND, 0)
        MainFrameSizer.Add(self.AdjustSettingsPanel, 0, wx.EXPAND, 0)
        MainFrameSizer.Add(self.CTCPanel, 0, wx.EXPAND, 0)
        MainFrameSizer.Add(self.MonitorPanel, 0, wx.EXPAND, 0)
        MainFrameSizer.Add(self.ControlPanel, 0, wx.EXPAND, 0)
        MainFrameSizer.Add(self.ReportsPanel, 0, wx.EXPAND, 0)
        MainFrameSizer.Add(self.AlarmsControlPanel, 0, wx.EXPAND, 0)
        MainFrameSizer.Add(self.AlarmThresholdPanel, 0, wx.EXPAND, 0)
        self.SetSizer(MainFrameSizer)
        self.Layout()

    def back(self, event):
        self.currentPanel.Hide()
        self.currentPanel = self.prevPanel
        self.prevPanel = self.prev2Panel
        self.prev2Panel = self.prev3Panel
        self.prev3Panel = self.prev4Panel
        self.currentPanel.Show()

    def home(self, event):
        self.currentPanel.Hide()
        self.currentPanel = self.HomePanel
        self.prevPanel = self.HomePanel
        self.prev2Panel = self.HomePanel
        self.prev3Panel = self.HomePanel
        self.prev4Panel = self.HomePanel
        self.currentPanel.Show()

    def login(self, event):
        if not self.LoginPanel.UsernameText.GetValue() == USERNAME:
            print 'INVALID USERNAME'
            return
        if not self.LoginPanel.PasswordText.GetValue() == PASSWORD:
            print 'INVALID PASSWORD'
            return
        else:
            self.LoginPanel.Hide()

            appmail.sendCustomEmailSubject("APPLICATIONLOGINSEND", "APPLICATION_\r\nLOGIN\r\n")

            while True:
                (applogin, newMail, messages) = appmail.getMail()

                if newMail and applogin:
                    for message in messages:

                        pass_read, message_read = message.split("_\r\n", 1)

                        try:
                            command, body = message_read.split('\r\n', 1)
                        except:
                            command = message_read.split('\r\n')

                        if pass_read == "APPLICATION" and command == "INITIALVALUES":
                            params = body.split('\r\n', 4)
                            self.AlarmThresholdPanel.HIGHTEMP_THRESHOLD = float(params[0])
                            self.AlarmThresholdPanel.LOWTEMP_THRESHOLD = float(params[1])
                            self.AlarmThresholdPanel.HIGHLEVEL_THRESHOLD = float(params[2])
                            self.AlarmThresholdPanel.LOWLEVEL_THRESHOLD = float(params[3])

                            self.AlarmHistoryPanel.AlarmHistory += params[4]

                            self.AlarmThresholdPanel.updateThresholdLabels()
                            self.AlarmHistoryPanel.updateAlarmHistory()
                            self.HomePanel.Show()
                            self.Layout()
                            return

    def changeEmailAddress(self, event):
        EMAIL_ADDRESS = self.AdjustSettingsPanel.EmailText.GetValue()
        self.UserInfoPanel.NotificationEmail.SetLabel("Email: " + EMAIL_ADDRESS)

        body = ""
        body += "APPLICATION_\r\n"
        body += "CHANGEEMAIL\r\n"
        body += EMAIL_ADDRESS + '\r\n'
        appmail.sendCustomEmail(body)

        self.currentPanel.Hide()
        self.currentPanel = self.prevPanel
        self.prevPanel = self.prev2Panel
        self.prev2Panel = self.prev3Panel
        self.prev3Panel = self.prev4Panel
        self.currentPanel.Show()

    def sendThresholdSettings(self, event):
        body = ""
        body += "APPLICATION_\r\n"
        body += "CHANGETHRESHOLD\r\n"
        body += str(self.AlarmThresholdPanel.HIGHTEMP_THRESHOLD) + '\r\n'
        body += str(self.AlarmThresholdPanel.LOWTEMP_THRESHOLD) + '\r\n'
        body += str(self.AlarmThresholdPanel.HIGHLEVEL_THRESHOLD) + '\r\n'
        body += str(self.AlarmThresholdPanel.LOWLEVEL_THRESHOLD) + '\r\n'
        appmail.sendCustomEmail(body)

        self.currentPanel.Hide()
        self.currentPanel = self.HomePanel
        self.prevPanel = self.HomePanel
        self.prev2Panel = self.HomePanel
        self.prev3Panel = self.HomePanel
        self.prev4Panel = self.HomePanel
        self.currentPanel.Show()

    def switchPanel(self, event, newpanel):
        self.currentPanel.Hide()
        self.prev4Panel = self.prev3Panel
        self.prev3Panel = self.prev2Panel
        self.prev2Panel = self.prevPanel
        self.prevPanel = self.currentPanel
        self.currentPanel = newpanel
        newpanel.Show()
        self.Layout()

    def sendCommand(self, event, command):
        appmail.sendEmailCommand(command + '\r\n')

class MyApp(wx.App):

    def OnInit(self):
        self.frame = MainFrame(None, wx.ID_ANY, "")
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
