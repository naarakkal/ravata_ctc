import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders
import email
import imaplib
import os
import time

fromaddr = "ctc@ravatasolutions.com"
password = "20205DCA"
toaddr = "ctc@ravatasolutions.com"

smtp_server = "smtp.gmail.com"
smtp_port = 587
imap_server = "imap.gmail.com"

def sendEmailCommand(command):
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = "APPLICATION"

    body = "APPLICATION_\r\n" + command

    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP(smtp_server, smtp_port)
    server.starttls()
    server.login(fromaddr, password)
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()

def sendCustomEmail(body):
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = "APPLICATION"

    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP(smtp_server, smtp_port)
    server.starttls()
    server.login(fromaddr, password)
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()

def sendCustomEmailSubject(subject, body):
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = subject

    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP(smtp_server, smtp_port)
    server.starttls()
    server.login(fromaddr, password)
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()


def getMail():
    mail = imaplib.IMAP4_SSL(imap_server)
    mail.login(fromaddr, password)
    mail.list()
    mail.select('inbox')
    body = []
    emailNum = 0
    newMail = False
    applogin = False
    (retcode, messages) = mail.search(None, '(UNSEEN)')
    if retcode == 'OK':
        for num in messages[0].split() :

            emailNum = emailNum + 1
            typ, data = mail.fetch(num,'(RFC822)')

            for response_part in data:
                if isinstance(response_part, tuple):
                    original = email.message_from_string(response_part[1])

                    if original['Subject'] == "APPLICATION" or original['Subject'] == "APPLICATIONLOGINRECEIVE":
                        print 'Processing new email'
                        if original['Subject'] == "APPLICATIONLOGINRECEIVE":
                            applogin = True
                        if original.is_multipart():
                            for part in original.walk():
                                ctype = part.get_content_type()
                                cdispo = str(part.get('Content-Disposition'))  # skip attachments
                                if ctype == 'text/plain' and 'attachment' not in cdispo:
                                    body.append(part.get_payload(decode=True))  # decode
                                    break
                        else:
                            body.append(original.get_payload(decode=True)) # not multipart - i.e. plain text, no attachments

                        print original['From']
                        print original['Subject']
                        print body
                        typ, data = mail.store(num,'+FLAGS','\\Seen')
                    else:
                    	typ, data = mail.store(num,'-FLAGS','\\Seen')
                    

    emailNum = emailNum - 1

# return a boolean value with whether a new a mail was received or not
    if emailNum >= 0:
        newMail = True
    else:
        newMail = False
    try:
        body.remove('\r\n')
    except:
        pass
    return (applogin, newMail, body)
