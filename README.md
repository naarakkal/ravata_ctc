# **README** #

This repo contains microcontroller source codes used by Ravata Solutions. 
Command to clone project:  
"git clone https://naarakkal@bitbucket.org/naarakkal/Ravata_CTC.git"

The programs in the ctc folder must be run on the CTC unit connected to the cryo-tank and the programs in the gui folder must be run on a desktop computer. The GUI application needs to be run from the command line using python.
More information on the code and the hardware involved for the implementation of the CTC can be found in the CTC documentaion and the user manual.