import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders
import email
import imaplib
import os
import time

# macros for setting up email services
fromaddr = "ctc@ravatasolutions.com"
password = "20205DCA"
toaddr = "ravatasolutions@gmail.com"

smtp_server = "smtp.gmail.com"
smtp_port = 587
imap_server = "imap.gmail.com"

def changeEmail(addr):
    global toaddr
    toaddr = addr

#sends an email whenever an alarm is triggered with the report attached
def sendMail(attachment, attachment2):

    subject = "Alarm triggered!"

    msg = MIMEMultipart()

    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = subject

    body = "A triggered alarm has been detected. For more details please see the attached report.\r\n\r\n"
    body += "To interact with the CTC by sending it commands, send an email back to this address with the following formatting.\r\n1. The subject must be 'Execute Command'\r\n2. Include the email password at the start of the body followed by a '_' character.\r\n3. Enter each command with a new line between each command\r\n\r\n"
    body += "For a complete list of commands refer to the command list attached in this email. A basic set of commands are also listed here. \r\nALMS 0 - Audible alarm off\r\nALMS 1 - Audible alarm on\r\nFILL 1 - Start filling\r\nFILL 0 - Stop filling\r\n\r\n\r\n"
    body += '*The attached report has been encrypted with PGP. To view the attachemnt please decrypt the file with a PGP decryption tool, key and PGP passphrase provided by your system administrator.*'
    msg.attach(MIMEText(body, 'plain'))

    filename = attachment
    attachment = open("./" + attachment, "rb")

    filename2 = attachment2
    attachment2 = open("./" + attachment2, "rb")

    part = MIMEBase('application', 'octet-stream')
    part.set_payload((attachment).read())
    encoders.encode_base64(part)

    # adds an attachment to the email
    part.add_header('Content-Disposition', "attachment; filename= %s" % filename)

    msg.attach(part)

    part2 = MIMEBase('application', 'octet-stream')
    part2.set_payload((attachment2).read())
    encoders.encode_base64(part2)

    # adds an attachment to the email
    part2.add_header('Content-Disposition', "attachment; filename= %s" % filename2)

    msg.attach(part2)

    server = smtplib.SMTP(smtp_server, smtp_port)
    server.starttls()
    server.login(fromaddr, password)
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()

# sends back a response with the commands sent via email
def sendResponse(response):
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = "Command(s) Executed"

    body = "The following commands have been executed and their responses have been recorded.\r\n\r\n"
    body += response
    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP(smtp_server, smtp_port)
    server.starttls()
    server.login(fromaddr, password)
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()

def sendEmailRaw(subject, body):
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = subject

    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP(smtp_server, smtp_port)
    server.starttls()
    server.login(fromaddr, password)
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()

def sendEmailSelfRaw(subject, body):
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = fromaddr
    msg['Subject'] = subject

    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP(smtp_server, smtp_port)
    server.starttls()
    server.login(fromaddr, password)
    text = msg.as_string()
    server.sendmail(fromaddr, fromaddr, text)
    server.quit()


# checks the subject of the email to filter out unwanted email processing
def checkExecuteCommand(string):
    string = ''.join(string.split())

    if string == 'executecommand' or string == 'ExecuteCommand' or string == 'Executecommand' or string == 'executeCommand':
        return True
    else:
        return False

# goes through the inbox to find unseen email, and parses its contents
def getMail():
    mail = imaplib.IMAP4_SSL(imap_server)
    mail.login(fromaddr, password)
    mail.list()
    mail.select('inbox')
    body = []
    emailNum = 0
    newMail = False
    application = False
    (retcode, messages) = mail.search(None, '(UNSEEN)')
    if retcode == 'OK':
        for num in messages[0].split():
            emailNum = emailNum + 1
            typ, data = mail.fetch(num,'(RFC822)')

            for response_part in data:
                if isinstance(response_part, tuple):
                    original = email.message_from_string(response_part[1])

                    if checkExecuteCommand(original['Subject']) or original['Subject'] == "APPLICATION" or original['Subject'] == "APPLICATIONLOGINSEND":
                        print 'Processing new email'
                        if original['Subject'] == 'APPLICATIONLOGINRECEIVE':
                            continue
                        if original['Subject'] == "APPLICATION" or original['Subject'] == "APPLICATIONLOGINSEND":
                            application = True
                        if original.is_multipart():
                            for part in original.walk():
                                ctype = part.get_content_type()
                                cdispo = str(part.get('Content-Disposition'))  # skip attachments
                                if ctype == 'text/plain' and 'attachment' not in cdispo:
                                    body.append(part.get_payload(decode=True))  # decode
                                    break
                        else:
                            body.append(original.get_payload(decode=True)) # not multipart - i.e. plain text, no attachments

                        print original['From']
                        print original['Subject']
                        print body
                        typ, data = mail.store(num,'+FLAGS','\\Seen')
                    

    emailNum = emailNum - 1

# return a boolean value with whether a new a mail was received or not
    if emailNum >= 0:
        newMail = True
    else:
        newMail = False
    try:
        body.remove('\r\n')
    except:
        pass
    return (newMail, body, application)
