import serial
import datetime
import time
import schedule
import gnupg
import TECmail

# set the serial port on the raspberry pi and the directory with gpg files
serialport = serial.Serial("/dev/serial0", baudrate=9600, timeout=0.2)
gpg = gnupg.GPG(gnupghome = './gpg/gpg')


email_password = 'abc' # set password for email at the beginning
mailCheckFrequency = 1 # interval to check mail (in seconds)
alarmCheckFrequency = 5 # interval to check alarms (in seconds)

pgpEmail = 'ravatasolutions@gmail.com'
pgpPassphrase = 'sweatequity'

previousAlarmStatus = []
currentAlarmStatus = []

# send a command through the serial port
def sendCommand(command):
    if command != "ALMS?":
        print command
    command += '\r\n'
    serialport.write(command)
    received_raw = serialport.read(50)
    received = received_raw.replace(" \r\n", "")
    received = received.replace("\n", "")
    received = received.replace("\r", "")
    return received

# check if any alarm is currently active and return a boolean value
def checkAlarmStatus():
    alarm = sendCommand("ALMS?")
    if alarm == '1':
        return True
    else:
        return False

# check which alarm was triggered by pinging all the alarm parameters
def pingAlarmCause():
    highTempAlarmA = sendCommand("HITAS?")
    highTempAlarmB = sendCommand("HITBS?")
    lowTempAlarmA = sendCommand("LOTAS?")
    lowTempAlarmB = sendCommand("LOTBS?")
    fillAlarm = sendCommand("FILAS?")
    highLevelAlarm = sendCommand("HILS?")
    lowLevelAlarm = sendCommand("LOLS?")

    #returns an array with all the alarm statuses
    alarmStatus = [highTempAlarmA, highTempAlarmB, lowTempAlarmA, lowTempAlarmB, fillAlarm,
            highLevelAlarm, lowLevelAlarm]
    return alarmStatus

def generateLogAndReport(alarmStatus):
    log = open("log.txt", "a+") # add tripped alarm to the log
    report = open("report.txt", "w+") # write report to be emailed

    timestamp = datetime.datetime.now()
    timestamp = str(timestamp)
    log.write(timestamp) # get date and time for each entry
    log.write("\n")
    report.write("\nREPORT\n")
    report.write(timestamp)
    report.write("\n\n\n")
    report.write("----------------------------\n")
    for i in range(0, 7): # check for each alarm that has been tripped and add to log/report
        if alarmStatus[i] == '1':
            if i == 0:
                log.write("High Temperature A Alarm triggered\n")
                report.write("High Temperature A Alarm triggered\n")
            if i == 1:
                log.write("High Temperature B Alarm triggered\n")
                report.write("High Temperature B Alarm triggered\n")
            if i == 2:
                log.write("Low Temperature A Alarm triggered\n")
                report.write("Low Temperature A Alarm triggered\n")
            if i == 3:
                log.write("Low Temperature B Alarm triggered\n")
                report.write("Low Temperature B Alarm triggered\n")
            if i == 4:
                log.write("Fill Alarm triggered\n")
                report.write("Fill Alarm triggered\n")
            if i == 5:
                log.write("High Level Alarm triggered\n")
                report.write("High Level Alarm triggered\n")
            if i == 6:
                log.write("Low Level Alarm triggered\n")
                report.write("Low Level Alarm triggered\n")
    report.write("----------------------------\n")
    log.write("\n")
    log.close()
    report.write("\n")

    tempUnit = sendCommand("TUNI?") #get temperature unit from TEC 2000
    levelChar = sendCommand("LUNI?") #get level character from TEC 2000

    #convert level character to its unit
    levelUnit = ""
    if levelChar == "E":
    	levelUnit = "inches"
    elif levelChar == "M":
        levelUnit = "mm"
    elif levelChar == "%":
        levelUnit = "%"

    #write to report and append to log
    curTempA = sendCommand("TEMPA?")
    if curTempA == "OPEN ":
        report.write("Current Probe A Temperature: Disconnected" + '\n')
    else:
        report.write("Current Probe A Temperature: " + curTempA + tempUnit + '\n')
    highTempA = sendCommand("HITA?")
    report.write("High Temperature A Alarm Threshold: " + highTempA + tempUnit + '\n')
    lowTempA = sendCommand("LOTA?")
    report.write("Low Temperature A Alarm Threshold: " + lowTempA + tempUnit + '\n\n')

    curTempB = sendCommand("TEMPB?")
    if curTempB == "OPEN ":
        report.write("Current Probe B Temperature: Disconnected" + '\n')
    else:
        report.write("Current Probe B Temperature: " + curTempB + tempUnit + '\n')
    highTempB = sendCommand("HITB?")
    report.write("High Temperature B Alarm Threshold: " + highTempB + tempUnit + '\n')
    lowTempB = sendCommand("LOTB?")
    report.write("Low Temperature B Alarm Threshold: " + lowTempB + tempUnit + '\n\n')

    bypassTemp = sendCommand("BPTMP?")
    if bypassTemp == "OPEN ":
        report.write("Hot Gas Bypass Temperature: Disconnected" + '\n\n')
    else:
        report.write("Hot Gas Bypass Temperature: " + bypassTemp + tempUnit + '\n\n')

    curLevel = sendCommand("LEVL?")
    report.write("Current Level: " + curLevel + ' ' + levelUnit + '\n')
    highLevelAlarm = sendCommand("HILA?")
    report.write("High Level Alarm Threshold: " + highLevelAlarm + ' ' + levelUnit + '\n')
    lowLevelAlarm = sendCommand("LOLA?")
    report.write("Low Level Alarm Threshold: " + lowLevelAlarm + ' ' + levelUnit + '\n\n')

    report.close()

    print "Encrypting report"

    # encrypt the report with gpg key
    with open('report.txt', 'rb') as encrypted_report:
        status = gpg.encrypt_file(encrypted_report, recipients=['ravatasolutions@gmail.com'], output='encrypted_report.txt.pgp')

def generateTempReport():
    timestamp = datetime.datetime.now()
    timestamp = str(timestamp)

    subject = "CTC Temperature Report at " + timestamp

    body = ""
    body += "Timestamp: " + timestamp + "\r\n"

    body += "\r\n"

    tempUnit = sendCommand("TUNI?") #get temperature unit from TEC 2000

    curTempA = sendCommand("TEMPA?")
    highTempA = sendCommand("HITA?")
    lowTempA = sendCommand("LOTA?")

    curTempB = sendCommand("TEMPB?")
    highTempB = sendCommand("HITB?")
    lowTempB = sendCommand("LOTB?")

    body += "Temperature Probe A: " + str(curTempA) + tempUnit + "\r\n"
    body += "Temperature Probe B: " + str(curTempB) + tempUnit + "\r\n"

    TECmail.sendEmailRaw(subject, body)

def generateFillReport():
    timestamp = datetime.datetime.now()
    timestamp = str(timestamp)

    subject = "CTC Fill Report at " + timestamp

    body = ""
    body += "Timestamp: " + timestamp + "\r\n"

    body += "\r\n"

    levelChar = sendCommand("LUNI?")

    levelUnit = ""
    if levelChar == "E":
    	levelUnit = "inches"
    elif levelChar == "M":
        levelUnit = "mm"
    elif levelChar == "%":
        levelUnit = "%"

    curLevel = sendCommand("LEVL?")
    highLevelAlarm = sendCommand("HILA?")
    lowLevelAlarm = sendCommand("LOLA?")

    if curLevel < highLevelAlarm or curLevel > lowLevelAlarm:
        body += "Summary: Fill Level is within thresholds.\r\n"
    else:
        body += "Summary: Fill Level is outside thresholds.\r\n"
    body += "Tank Fill Level: " + str(curLevel) + levelUnit + "\r\n"

    TECmail.sendEmailRaw(subject, str(body))

def generateAlarmReport():
    didAlarmTrip = False
    timestamp = datetime.datetime.now()
    timestamp = str(timestamp)

    subject = "CTC Alarm Report at " + timestamp

    body = ""
    body += "Timestamp: " + timestamp + "\r\n"

    body += "\r\n"
    body += "Alarm Summary: "

    if currentAlarmStatus != []:
        for i in range(0, 7): # check for each alarm that has been tripped and add to log/report
            if currentAlarmStatus[i] == '1':
                didAlarmTrip = True
                if i == 0:
                    body += ("High Temperature A Alarm triggered\r\n")
                if i == 1:
                    body += ("High Temperature B Alarm triggered\r\n")
                if i == 2:
                    body += ("Low Temperature A Alarm triggered\r\n")
                if i == 3:
                    body += ("Low Temperature B Alarm triggered\r\n")
                if i == 4:
                    body += ("Fill Alarm triggered\r\n")
                if i == 5:
                    body += ("High Level Alarm triggered\r\n")
                if i == 6:
                    body += ("Low Level Alarm triggered\r\n")

    if not didAlarmTrip:
        body += "No alarm tripped\r\n"

    body += "Users Notified: "

    TECmail.sendEmailRaw(subject, str(body))

def generateHistory():
    timestamp = datetime.datetime.now()
    timestamp = str(timestamp)

    subject = "CTC History at " + timestamp

    body = ""
    with open('log.txt') as log:
        for line in log.readlines():
            body += line

    TECmail.sendEmailRaw(subject, str(body))

def applicationMuteAlarm():
    print "Muting MVE TEC 3000"
    sendCommand('ALMS 0')

def applicationStartFill():
    print "Starting Fill"
    sendCommand('FILL 1')

def applicationStopFill():
    print "Stopping Fill"
    sendCommand('FILL 0')

def applicationChangeThreshold(body):
    stringparams = body.split('\r\n')
    params = [float(stringparams[0]), float(stringparams[1]), float(stringparams[2]), float(stringparams[3])]
    sendCommand("HITA " + str(params[0]))
    sendCommand("LOTA " + str(params[1]))
    sendCommand("HILA " + str(params[2]))
    sendCommand("LOLA " + str(params[3]))

def applicationLogIn():
    params = []
    params.append(sendCommand("HITA?"))
    params.append(sendCommand("LOTA?"))
    params.append(sendCommand("HILA?"))
    params.append(sendCommand("LOLA?"))

    subject = "APPLICATIONLOGINRECEIVE"

    body = ""
    body += "APPLICATION_" + "\r\n"
    body += "INITIALVALUES" + "\r\n"
    body += params[0] + "\r\n"
    body += params[1] + "\r\n"
    body += params[2] + "\r\n"
    body += params[3] + "\r\n"

    with open('log.txt') as log:
        for line in log.readlines():
            body += line

    TECmail.sendEmailSelfRaw(subject, body)

def applicationChangeEmail(email):
    email = email.replace("\r\n", "")
    TECmail.changeEmail(email)

def addCommandsToLog(commands):
    log = open("log.txt", "a+")
    timestamp = datetime.datetime.now()
    timestamp = str(timestamp)
    log.write(timestamp) # get date and time for each entry
    log.write("\n")
    print 'Appending commands executed to log'
    log.write(commands)
    log.close()

#decrypt mail received, process commands and create a response with commands executed
def processMail(messages, email_password):

    # decrypt all the messages received
    for i in range(len(messages)):
        try:
            messages[i] = str(gpg.decrypt(messages[i], passphrase = pgpPassphrase))
        except:
            pass
    # check if the body of email contains the password followed by an '_' character
    for message in messages:
        pass_read, message_read = message.split("_\n", 1)
        commands = message_read.split('\r\n')

        if pass_read != email_password:
            print "Incvalid Password entered in email"
            continue

        # split rest of the body into an individual list of commands and send the commands
        for command in commands:

            if command == '' or command == '\r\n' or command == ' ' or command == '\r' or command == '\n':
                continue
            print "Sending command - " + command
            response = sendCommand(command)
            print response

            #create response with commands executed
            body += "Command: "
            body += command
            body += '\r\n'
            body += "Response: "
            if response == '' or response == '\r\n' or response == ' ' or response == '\r' or response == '\n':
                body += "No response received (not a query)"
            else:
                body += response
            body += '\r\n\r\n'


        # encrypt the response email before sending
        addCommandsToLog(body)
        encrypted_body = gpg.encrypt(body, pgpEmail)
        TECmail.sendResponse(str(encrypted_body))

def processApplication(messages, email_password):

    # check if the body of email contains the password followed by an '_' character
    for message in messages:
        pass_read, message_read = message.split("_\r\n", 1)
        try:
            command, body = message_read.split('\r\n', 1)
        except:
            tempcommand = message_read.split("\r\n")
            command = tempcommand[0]


        if pass_read == "APPLICATION":
            if command == '' or command == '\r\n' or command == ' ' or command == '\r' or command == '\n':
                continue
            if command == "SENDTEMPREPORT":
                generateTempReport()
            if command == "SENDFILLREPORT":
                generateFillReport()
            if command == "SENDALARMREPORT":
                generateAlarmReport()
            if command == "MUTEALARM":
                applicationMuteAlarm()
            if command == "STARTFILL":
                applicationStartFill()
            if command == "STOPFILL":
                applicationStopFill()
            if command == "CHANGETHRESHOLD":
                applicationChangeThreshold(body)
            if command == "CHANGEEMAIL":
                applicationChangeEmail(body)
            if command == "LOGIN":
                applicationLogIn()
            if command == "SENDHISTORY":
                generateHistory()


# checks for new emails, and sends them to process if a new email was received
def checkMail():
    (newMail, messages, application) = TECmail.getMail()
    if application:
        processApplication(messages, email_password)
        return
    if newMail:
        try:
            processMail(messages, email_password)
        except:
            pass

# checks for new alarms triggered
def checkAlarm():
    global currentAlarmStatus
    global previousAlarmStatus

    if checkAlarmStatus():
        if currentAlarmStatus:
            previousAlarmStatus = currentAlarmStatus
        currentAlarmStatus = pingAlarmCause()

        if previousAlarmStatus == currentAlarmStatus:
            return
        print("Alarm Triggered, generating report")
        generateLogAndReport(currentAlarmStatus)
        print("Report generated, sending email")
        TECmail.sendMail("encrypted_report.txt.pgp", "command_list.txt")

#schedule the frequency of each of the tasks
schedule.every(mailCheckFrequency).seconds.do(checkMail)
#schedule.every(alarmCheckFrequency).seconds.do(checkAlarm)

while True:
    schedule.run_pending()
    time.sleep(1)
