#!/usr/bin/python

import serial
import time

serialport = serial.Serial("/dev/serial0", baudrate=9600, timeout=0.2)

print("Starting serial comm")

while True:
    command = raw_input("Enter a command: ")
    command += '\r\n'
    serialport.write(command)
    time.sleep(0.25)
    print serialport.read(100)
