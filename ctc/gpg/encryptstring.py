import gnupg

gpg = gnupg.GPG(gnupghome='./')
unencrypted_string = 'PGP Encryption is the best form of encrypting data in the world.\r\n\r\nyolo!'
encrypted_data = gpg.encrypt(unencrypted_string, 'naraklol@ravatasolutions.com')
encrypted_string = str(encrypted_data)
print 'ok: ', encrypted_data.ok
print 'status: ', encrypted_data.status
print 'stderr: ', encrypted_data.stderr
print 'unencrypted_string: ', unencrypted_string
print 'encrypted_string: ', encrypted_string
