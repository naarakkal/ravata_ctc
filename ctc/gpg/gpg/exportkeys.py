import gnupg

key = '035EA0C27BCC1D31'
gpg = gnupg.GPG(gnupghome='./')
ascii_armored_public_keys = gpg.export_keys(key, passphrase='sweatequity')
ascii_armored_private_keys = gpg.export_keys(key, True, passphrase='sweatequity')
with open('keys.asc', 'w') as f:
    f.write(ascii_armored_public_keys)
    f.write(ascii_armored_private_keys)
